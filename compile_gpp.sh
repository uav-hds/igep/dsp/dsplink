unset LDFLAGS

function green_echo ()
{
    echo -e "\033[32m$1\033[0m"
}

function red_echo ()
{
    echo -e "\033[31m$1\033[0m"
}

function check_error ()
{
    if [ "$?" != "0" ]; then
        red_echo "Erreur, arret"
        exit 1
    fi
}

green_echo "Configuring dsplink"
cd $TI_TOOLS_BASE_DIR
cd dsplink/dsplink/config/bin
perl dsplinkcfg.pl --platform=OMAP3530 --nodsp=1 --dspcfg_0=OMAP3530SHMEM --dspos_0=DSPBIOS5XX --gppos=OMAPLSP --comps=PONM --DspTskMode=1 > /dev/null
check_error

green_echo "Compilation dsplink gpp"
cd ../../gpp/src
make clean
make release
check_error
#cp ../export/BIN/Linux/OMAP3530/RELEASE/dsplinkk.ko $IGEP_ROOT/uav_dev/bin/arm
#cp -R ../export/INCLUDE/Linux/OMAP3530/usr $IGEP_ROOT/uav_dev/include/dsplink
#cp ../inc/sys/Linux/*.h $IGEP_ROOT/uav_dev/include/dsplink
#cp ../BUILD/EXPORT/RELEASE/dsplink.o $IGEP_ROOT/uav_dev/lib/arm

